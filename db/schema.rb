# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_09_11_192128) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bikes", force: :cascade do |t|
    t.string "make"
    t.string "model"
    t.integer "year"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "image_url"
    t.index ["user_id"], name: "index_bikes_on_user_id"
  end

  create_table "conversations", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "friend_rides", force: :cascade do |t|
    t.bigint "friend_id"
    t.bigint "ride_id"
    t.index ["friend_id"], name: "index_friend_rides_on_friend_id"
    t.index ["ride_id"], name: "index_friend_rides_on_ride_id"
  end

  create_table "friends", force: :cascade do |t|
    t.string "username"
    t.text "avatar"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "miles_ridden_together"
    t.index ["user_id"], name: "index_friends_on_user_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.string "text"
    t.bigint "conversation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["conversation_id"], name: "index_messages_on_conversation_id"
  end

  create_table "rides", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.integer "distance"
    t.string "ride_category"
    t.integer "duration"
    t.string "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "image_link"
    t.text "map_link"
  end

  create_table "user_groups", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_user_groups_on_group_id"
    t.index ["user_id"], name: "index_user_groups_on_user_id"
  end

  create_table "user_rides", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "ride_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ride_id"], name: "index_user_rides_on_ride_id"
    t.index ["user_id"], name: "index_user_rides_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "username"
    t.string "password_digest"
    t.string "email"
    t.bigint "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "avatar"
    t.text "about"
    t.text "background_image"
  end

  add_foreign_key "bikes", "users"
  add_foreign_key "friend_rides", "friends"
  add_foreign_key "friend_rides", "rides"
  add_foreign_key "friends", "users"
  add_foreign_key "messages", "conversations"
  add_foreign_key "user_groups", "groups"
  add_foreign_key "user_groups", "users"
  add_foreign_key "user_rides", "rides"
  add_foreign_key "user_rides", "users"
end
