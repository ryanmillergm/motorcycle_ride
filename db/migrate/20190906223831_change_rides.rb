class ChangeRides < ActiveRecord::Migration[5.2]
  def change
    rename_column :rides, :geography, :type
    add_column :rides, :image_url, :text
    add_column :rides, :map_url, :text
  end
end
