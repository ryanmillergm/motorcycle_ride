class CreateUserRides < ActiveRecord::Migration[5.2]
  def change
    create_table :user_rides do |t|
      t.references :user, foreign_key: true
      t.references :ride, foreign_key: true

      t.timestamps
    end
  end
end
