class AddColumnToFriends < ActiveRecord::Migration[5.2]
  def change
    add_column :friends, :miles_ridden_together, :integer
  end
end
