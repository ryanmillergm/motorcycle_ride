class CreateRides < ActiveRecord::Migration[5.2]
  def change
    create_table :rides do |t|
      t.string :title
      t.text :description
      t.integer :distance
      t.string :geography
      t.integer :duration
      t.string :date

      t.timestamps
    end
  end
end
