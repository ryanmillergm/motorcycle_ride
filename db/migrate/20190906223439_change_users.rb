class ChangeUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :avatar, :text
    add_column :users, :about, :text
    add_column :users, :background_image, :text
  end
end
