class ChangeColumnNamesOnRides < ActiveRecord::Migration[5.2]
  def change
    rename_column :rides, :ride_type, :ride_category
  end
end
