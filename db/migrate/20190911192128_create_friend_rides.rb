class CreateFriendRides < ActiveRecord::Migration[5.2]
  def change
    create_table :friend_rides do |t|
      t.references :friend, foreign_key: true
      t.references :ride, foreign_key: true
    end
  end
end
