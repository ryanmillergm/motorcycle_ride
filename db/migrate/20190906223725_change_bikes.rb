class ChangeBikes < ActiveRecord::Migration[5.2]
  def change
    add_column :bikes, :image_url, :text
  end
end
