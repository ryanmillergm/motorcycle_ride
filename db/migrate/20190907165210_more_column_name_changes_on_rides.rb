class MoreColumnNameChangesOnRides < ActiveRecord::Migration[5.2]
  def change
    rename_column :rides, :image_url, :image_link
    rename_column :rides, :map_url, :map_link
  end
end
