require 'rails_helper'

RSpec.describe 'Forecasts' do
  it 'gets latitude and longitude from geocoding' do
    VCR.use_cassette('forecast_request') do

      get '/api/v1/forecast?location=denver,co&time=1568143685'

      expect(response).to be_successful
      weather = JSON.parse(response.body, symbolize_names: true)

      attributes = [:id, :time, :summary, :icon, :precip_probablility, :temperature, :apparent_temperature, :humidity, :uv_index, :visibility, :wind_speed]
      expect(weather[:data][:attributes][:location]).to eq("denver,co")
      expect(weather[:data][:attributes][:get_forecast].keys).to match_array(attributes)
    end
  end

  it 'gets latitude and longitude from geocoding' do
    VCR.use_cassette('forecast_different_location_request') do

      get '/api/v1/forecast?location=aurora,co&time=1568143685'

      expect(response).to be_successful

      weather = JSON.parse(response.body, symbolize_names: true)

      expect(weather[:data][:attributes][:location]).to eq("aurora,co")
    end
  end

end
