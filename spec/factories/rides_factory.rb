FactoryBot.define do
  factory :ride do
    sequence(:title) { |n| "title-#{n}" }
    sequence(:description) { |n| "description-#{n}" }
    sequence(:distance) { |n| n }
    sequence(:ride_category) { |n| "ride_category-#{n}" }
    sequence(:duration) { |n| n }
    sequence(:date) { |n| "date-#{n}" }
    sequence(:image_link) { |n| "image_link-#{n}" }
    sequence(:map_link) { |n| "map_link-#{n}" }
  end
end
