FactoryBot.define do
  factory :group do
    sequence(:title) { |n| "title-#{n}" }
    sequence(:description) { |n| "description-#{n}" }
  end
end
