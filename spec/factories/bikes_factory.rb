FactoryBot.define do
  factory :bike do
    sequence(:make) { |n| "make-#{n}" }
    sequence(:model) { |n| "model-#{n}" }
    sequence(:year) { |n| n }
    sequence(:user_id) { |n| n }
    user
  end
end
