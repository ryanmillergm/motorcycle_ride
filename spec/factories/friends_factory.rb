FactoryBot.define do
  factory :friend do
    sequence(:username) { |n| "john_doe#{n}" }
    sequence(:avatar) { |n| "picture-#{n}" }
    sequence(:user_id) { |n| n }
    user
  end
end
