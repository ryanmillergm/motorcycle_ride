# require 'rails_helper'
#
# RSpec.describe 'As a visitor' do
#   describe 'visitor can register to create an account' do
#     let(:query) do
#       mutation {
#         createUser(
#           firstName: "Bobby",
#           lastName: "McGee",
#           username: "bobby",
#           email: "bobby@gmail",
#           phone: 2343424,
#           avatar: "image",
#           about: "Me",
#           backgroundImage: "image"
#         ){
#           firstName
#           lastName
#           username
#           email
#           phone
#         }
#       }
#     end
#     subject(:result) do
#       MotorcycleRideSchema.execute(query).as_json
#     end
#
#     it "creates a user" do
#       expected = {
#         "firstName" => "Bobby",
#         "lastName" => "McGee",
#         "username" => "bobby",
#         "email" => "bobby@gmail",
#         "phone" => 2343424,
#         "avatar" => "image",
#         "about" => "Me",
#         "backgroundImage" => "image"
#       }
#       user_result = result.dig("data", "user")
#
#       expect(user_result["firstName"]).to eq(expected.first_name)
#       expect(user_result["lastName"]).to eq(expected.last_name)
#       expect(user_result["username"]).to eq(expected.username)
#       expect(user_result["email"]).to eq(expected.email)
#       expect(user_result["phone"]).to eq(expected.phone)
#     end
#   end
# end
