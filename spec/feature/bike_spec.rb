# require "rails_helper"
#
# RSpec.describe Bike, type: :request do
#   describe 'user makes a GET graphql request' do
#     it 'and gets a listing of all bikes' do
#       # User seeds
#       user1 = User.create!(first_name: "Kermit", last_name: "The Frog", username: "Kermie", email: "kermit@muppets.com", phone: 1234567890)
#       user2 = User.create!(first_name: "Fozzy", last_name: "The Bear", username: "The Fozz", email: "fozzie@muppets.com", phone: 1112223333)
#       user3 = User.create!(first_name: "Miss", last_name: "Piggy", username: "Pink Pig", email: "piggy@muppets.com", phone: 1114445555)
#       user4 = User.create!(first_name: "Swedish", last_name: "Chef", username: "Cook", email: "chef@muppets.com", phone: 1116667777)
#       user5 = User.create!(first_name: "Statler", last_name: "Waldorf", username: "Grumpy", email: "grouches@muppets.com", phone: 1118889999)
#       user6 = User.create!(first_name: "Beaker", last_name: "Meep", username: "test_subject", email: "beaker@muppets.com", phone: 2223334444)
#       user7 = User.create!(first_name: "Dr. Bunsen", last_name: "Honeydew", username: "scientist", email: "honeydew@muppets.com", phone: 2224445555)
#       user8 = User.create!(first_name: "Gonzo", last_name: "The Great", username: "exploding trumpet", email: "gonzo@muppets.com", phone: 2226667777)
#       user9 = User.create!(first_name: "Animal", last_name: "Drummer", username: "Mayhem", email: "animal@muppets.com", phone: 2228889999)
#       user10 = User.create!(first_name: "Rowlf", last_name: "The Dog", username: "Good Boy", email: "rowlf@muppets.com", phone: 3332223333)
#
#       # Bike seeds
#       bike1 = user1.bikes.create!(make: "Honda", model: "GoldWing", year: 2018)
#       bike2 = user2.bikes.create!(make: "Kawasaki", model: "Ninja", year: 2019)
#       bike3 = user3.bikes.create!(make: "Suzuki", model: "Hyabusa", year: 2018)
#       bike4 = user4.bikes.create!(make: "Yamaha", model: "V Max", year: 2008)
#       bike5 = user5.bikes.create!(make: "Harley", model: "Electra Glide", year: 2010)
#       bike6 = user6.bikes.create!(make: "Triumph", model: "Speed Triple", year: 2014)
#       bike7 = user7.bikes.create!(make: "BMW", model: "LT1100", year: 2005)
#       bike8 = user8.bikes.create!(make: "Indian", model: "Scout", year: 2018)
#       bike9 = user9.bikes.create!(make: "Ducati", model: "Hypermotard", year: 2017)
#       bike10 = user10.bikes.create!(make: "KTM", model: "690 Enduro", year: 2016)
#
#       get('/graphql', params: {query: {"allBikes" => {"id"}}})
#       # get '/graphql?query={allBikes{id,make,model,year}}'
#
#       results = JSON.parse(response.body)
#
#       expect(results["data"]).to have_key("allBikes")
#       expect(results["data"]["allBikes"]).to have_key("id")
#       expect(results["data"]["allBikes"]).to have_key("make")
#       expect(results["data"]["allBikes"]).to have_key("model")
#       expect(results["data"]["allBikes"]).to have_key("year")
#     end
#   end
# end
