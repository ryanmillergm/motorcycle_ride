require "rails_helper"

RSpec.describe Types::QueryType do
  describe "rides" do
    let!(:rides) { create_pair(:ride) }

    let(:query) do
      %(query {
          allRides {
            title
            description
            distance
            rideCategory
            duration
            date
            imageLink
          }
      })
    end
    subject(:result) do
      MotorcycleRideSchema.execute(query).as_json
    end

    it "returns all rides" do
      expect(result.dig("data", "allRides")).to match_array(
        rides.map do |ride|
          {
            "title" => ride.title,
            "description" => ride.description,
            "distance" => ride.distance,
            "rideCategory" => ride.ride_category,
            "duration" => ride.duration,
            "date" => ride.date,
            "imageLink" => ride.image_link
          }
        end
      )
    end
  end
end
