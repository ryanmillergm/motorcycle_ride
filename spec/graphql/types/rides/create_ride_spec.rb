require "rails_helper"

RSpec.describe Types::QueryType do
  describe "get ride by id" do

    let(:createRide) do
      %(mutation {
        createRide(
          title: "Mt Evans Tour",
          description: "We're going to cruise up to Mt Evans",
          distance: 75,
          rideCategory: "Cruising",
          duration: 2,
          date: "7/12/20",
          imageLink: "image",
          mapLink: "google.maps"
        ){
          title
          description
          distance
          rideCategory
          duration
          date
          imageLink
          mapLink
        }
      })
    end

    subject(:result) do
      MotorcycleRideSchema.execute(createRide).as_json
    end

    it "returns adds a new ride" do
      ride_result = result["data"]["createRide"]

      expect(ride_result["title"]).to eq("Mt Evans Tour")
      expect(ride_result["description"]).to eq("We're going to cruise up to Mt Evans")
      expect(ride_result["distance"]).to eq(75)
      expect(ride_result["rideCategory"]).to eq("Cruising")
      expect(ride_result["duration"]).to eq(2)
      expect(ride_result["date"]).to eq("7/12/20")
      expect(ride_result["imageLink"]).to eq("image")
      expect(ride_result["mapLink"]).to eq("google.maps")
    end
  end
end
