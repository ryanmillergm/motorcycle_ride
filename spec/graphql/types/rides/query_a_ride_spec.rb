require "rails_helper"

RSpec.describe Types::QueryType do
  describe "get ride by id" do

    let!(:ride) { Ride.create(id: 1, title: 'Mountains', description: 'lots of ups and downs', distance: 57, ride_category: 'touring', duration: 4, date: '12July19', image_link: 'link to picture', map_link: 'link to map') }
    let!(:ride2) { Ride.create(id: 2, title: 'Open Road', description: 'flat and long straightaways', distance: 123, ride_category: 'day ride', duration: 5, date: '15May19', image_link: 'link to picture', map_link: 'link to map') }
    let!(:ride3) { Ride.create(id: 3, title: 'Trail Ride', description: 'national forest dirt bike trails', distance: 21, ride_category: 'dirt', duration: 6, date: '11Aug18', image_link: 'link to picture', map_link: 'link to map') }

    let(:query) do
      %(query {
        ride(id: "2") {
          title
          description
          distance
          rideCategory
          duration
          date
          imageLink
          mapLink
        }
      })
    end
    
    subject(:result) do
      MotorcycleRideSchema.execute(query).as_json
    end

    it "returns a ride by id" do
      expected = {
        "title" => ride.title,
        "description" => ride.description,
        "distance" => ride.distance,
        "rideCategory" => ride.ride_category,
        "duration" => ride.duration,
        "date" => ride.date,
        "imageLink" => ride.image_link,
        "mapLink" => ride.map_link
      }
      ride_result = result.dig("data", "ride")

      expect(ride_result["title"]).to eq(ride2.title)
      expect(ride_result["description"]).to eq(ride2.description)
      expect(ride_result["distance"]).to eq(ride2.distance)
      expect(ride_result["rideCategory"]).to eq(ride2.ride_category)
      expect(ride_result["duration"]).to eq(ride2.duration)
      expect(ride_result["date"]).to eq(ride2.date)
      expect(ride_result["imageLink"]).to eq(ride2.image_link)
      expect(ride_result["mapLink"]).to eq(ride2.map_link)
    end
  end
end
