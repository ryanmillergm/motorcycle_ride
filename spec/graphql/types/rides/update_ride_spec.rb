require "rails_helper"

RSpec.describe Types::QueryType do
  describe "update a ride" do
    let!(:ride) { Ride.create(id: 1, title: 'Mountains', description: 'lots of ups and downs', distance: 57, ride_category: 'touring', duration: 4, date: '12July19', image_link: 'link to picture', map_link: 'link to map') }
    let!(:ride2) { Ride.create(id: 2, title: 'Open Road', description: 'flat and long straightaways', distance: 123, ride_category: 'day ride', duration: 5, date: '15May19', image_link: 'link to picture', map_link: 'link to map') }
    let!(:ride3) { Ride.create(id: 3, title: 'Trail Ride', description: 'national forest dirt bike trails', distance: 21, ride_category: 'dirt', duration: 6, date: '11Aug18', image_link: 'link to picture', map_link: 'link to map') }

    let(:query) do
      %(mutation {
        updateRide(
          id: 1
          description: "Ride until you die!"
        )
        {
          title
          description
          distance
          rideCategory
          duration
          date
          imageLink
          mapLink
        }
      })
    end
    subject(:result) do
      MotorcycleRideSchema.execute(query).as_json
    end

    it "updates a user by id" do
      user_result = result.dig("data", "updateRide")

      expect(user_result["description"]).to eq("Ride until you die!")
    end
  end
end
