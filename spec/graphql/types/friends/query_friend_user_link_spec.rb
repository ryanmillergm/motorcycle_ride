require "rails_helper"

RSpec.describe Types::QueryType do
  describe "gets a friends users" do
    let!(:user) { User.create!(first_name: "Kermit", last_name: "The Frog", username: "Kermie", password: 'password', email: "kermit@muppets.com", phone: 1234567890, about: 'timid green rider', avatar: 'https://66.media.tumblr.com/avatar_f5b7729fb645_128.pnj', background_image: 'https://www.motorcycle.com/blog/wp-content/uploads/2017/05/050917-great-motorcycle-rides-north-america-f.jpg') }
    let!(:user2) { User.create!(first_name: "Fozzy", last_name: "The Bear", username: "The Fozz", password: 'password', email: "fozzie@muppets.com", phone: 1112223333, about: 'likes casual rides', avatar: 'https://pm1.narvii.com/6300/279717e1eaf7c5ed351ed1272ac99814834a1182_128.jpg', background_image: 'https://visitpa.com/sites/default/master/files/styles/960/public/carousel/feature-2-LP-image.jpg?itok=lw1z_kvP') }
    let!(:user3) { User.create!(first_name: "Miss", last_name: "Piggy", username: "Pink Pig", password: 'password', email: "piggy@muppets.com", phone: 1114445555, about: 'dont slow her down', avatar: 'https://pm1.narvii.com/7092/d9c207406796315e5e19389ff94c95066c3c592ar1-474-474v2_128.jpg', background_image: 'http://mag.gothrider.com/wp-content/uploads/2016/06/klickitatloop.jpg') }

    let!(:friend) { Friend.create(id: 1, username: "Luke", avatar: "pic1", user_id: user2.id) }
    let!(:friend2) { Friend.create(id: 2, username: "Leia", avatar: "pic2", user_id: user2.id) }
    let!(:friend3) { Friend.create(id: 3, username: "Obi Wan", avatar: "pic3", user_id: user3.id) }

    let(:query) do
      %(query {
        friend(id: "2") {
          username
          avatar
          user {
            firstName
            lastName
            username
            email
            phone
          }
        }
      })
    end
    subject(:result) do
      MotorcycleRideSchema.execute(query).as_json
    end

    it "returns the user for a friend" do
      expected = {
        "firstName" => user.first_name,
        "lastName" => user.last_name,
        "username" => user.username,
        "email" => user.email,
        "phone" => user.phone,
        "username" => friend.username,
        "avatar" => friend.avatar
      }
      friend_result = result.dig("data", "friend")

      expect(friend_result["username"]).to eq(friend2.username)
      expect(friend_result["avatar"]).to eq(friend2.avatar)
      expect(friend_result["user"]["lastName"]).to eq(user2.last_name)
      expect(friend_result["user"]["firstName"]).to eq(user2.first_name)
      expect(friend_result["user"]["username"]).to eq(user2.username)
      expect(friend_result["user"]["email"]).to eq(user2.email)
      expect(friend_result["user"]["phone"]).to eq(user2.phone)
    end
  end
end
