require "rails_helper"

RSpec.describe Types::QueryType do
  describe "gets a friends rides" do
    let!(:user) { User.create!(first_name: "Kermit", last_name: "The Frog", username: "Kermie", password: 'password', email: "kermit@muppets.com", phone: 1234567890, about: 'timid green rider', avatar: 'https://66.media.tumblr.com/avatar_f5b7729fb645_128.pnj', background_image: 'https://www.motorcycle.com/blog/wp-content/uploads/2017/05/050917-great-motorcycle-rides-north-america-f.jpg') }
    let!(:user2) { User.create!(first_name: "Fozzy", last_name: "The Bear", username: "The Fozz", password: 'password', email: "fozzie@muppets.com", phone: 1112223333, about: 'likes casual rides', avatar: 'https://pm1.narvii.com/6300/279717e1eaf7c5ed351ed1272ac99814834a1182_128.jpg', background_image: 'https://visitpa.com/sites/default/master/files/styles/960/public/carousel/feature-2-LP-image.jpg?itok=lw1z_kvP') }
    let!(:user3) { User.create!(first_name: "Miss", last_name: "Piggy", username: "Pink Pig", password: 'password', email: "piggy@muppets.com", phone: 1114445555, about: 'dont slow her down', avatar: 'https://pm1.narvii.com/7092/d9c207406796315e5e19389ff94c95066c3c592ar1-474-474v2_128.jpg', background_image: 'http://mag.gothrider.com/wp-content/uploads/2016/06/klickitatloop.jpg') }

    let!(:friend) { Friend.create(id: 1, username: "Luke", avatar: "pic1", user_id: user2.id) }
    let!(:friend2) { Friend.create(id: 2, username: "Leia", avatar: "pic2", user_id: user2.id) }
    let!(:friend3) { Friend.create(id: 3, username: "Obi Wan", avatar: "pic3", user_id: user3.id) }

    let!(:ride) { Ride.create(id: 1, title: 'Mountains', description: 'lots of ups and downs', distance: 57, ride_category: 'touring', duration: 4, date: '12July19', image_link: 'link to picture', map_link: 'link to map') }
    let!(:ride2) { Ride.create(id: 2, title: 'Open Road', description: 'flat and long straightaways', distance: 123, ride_category: 'day ride', duration: 5, date: '15May19', image_link: 'link to picture', map_link: 'link to map') }
    let!(:ride3) { Ride.create(id: 3, title: 'Trail Ride', description: 'national forest dirt bike trails', distance: 21, ride_category: 'dirt', duration: 6, date: '11Aug18', image_link: 'link to picture', map_link: 'link to map') }

    let!(:friend_ride) { FriendRide.create!(id: 1, friend_id: friend.id, ride_id: ride.id) }
    let!(:friend_ride) { FriendRide.create!(id: 1, friend_id: friend.id, ride_id: ride2.id) }
    let!(:friend_ride) { FriendRide.create!(id: 1, friend_id: friend.id, ride_id: ride3.id) }

    let(:query) do
      %(query {
        friend(id: "1") {
          username
          avatar
          rides {
            title
            description
            distance
            rideCategory
            duration
            date
            imageLink
            mapLink
          }
        }
      })
    end
    subject(:result) do
      MotorcycleRideSchema.execute(query).as_json
    end

    it "returns the rides for a friend" do
      friend_result = result.dig("data", "friend")

      expect(friend_result["rides"][0]["title"]).to eq(ride3.title)
      expect(friend_result["rides"][0]["description"]).to eq(ride3.description)
      expect(friend_result["rides"][0]["duration"]).to eq(ride3.duration)
      expect(friend_result["rides"][0]["date"]).to eq(ride3.date)
    end
  end
end
