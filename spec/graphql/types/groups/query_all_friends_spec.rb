require "rails_helper"

RSpec.describe Types::QueryType do
  describe "friends" do
    let!(:friends) { create_pair(:friend) }

    let(:query) do
      %(query {
          allFriends {
              username
              avatar
          }
      })
    end
    subject(:result) do
      MotorcycleRideSchema.execute(query).as_json
    end

    it "returns all friends" do
      expect(result.dig("data", "allFriends")).to match_array(
        friends.map do |friend|
          {
            "username" => friend.username,
            "avatar" => friend.avatar
          }
        end
      )
    end
  end
end
