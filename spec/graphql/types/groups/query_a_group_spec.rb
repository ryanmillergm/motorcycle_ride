require "rails_helper"

RSpec.describe Types::QueryType do
  describe "get group by id" do
    let!(:group) { Group.create(id: 1, title: "Dirtbikes", description: "We ride dirtbikes") }
    let!(:group2) { Group.create(id: 2, title: "Hogs", description: "We ride hogs") }
    let!(:group3) { Group.create(id: 3, title: "Rice Rockets", description: "We ride rice rockets") }

    let(:query) do
      %(query {
        group(id: "2") {
          title
          description
        }
      })
    end
    subject(:result) do
      MotorcycleRideSchema.execute(query).as_json
    end

    it "returns a group by id" do
      expected = {
        "title" => group.title,
        "description" => group.description,
      }
      group_result = result.dig("data", "group")

      expect(group_result["title"]).to eq(group2.title)
      expect(group_result["description"]).to eq(group2.description)
    end
  end
end
