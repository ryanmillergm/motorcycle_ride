require "rails_helper"

RSpec.describe Types::QueryType do
  describe "a user can create a group" do

    let!(:user) { User.create!(id: 1, first_name: "Kermit", last_name: "The Frog", username: "Kermie", password: 'password', email: "kermit@muppets.com", phone: 1234567890, about: 'timid green rider', avatar: 'https://66.media.tumblr.com/avatar_f5b7729fb645_128.pnj', background_image: 'https://www.motorcycle.com/blog/wp-content/uploads/2017/05/050917-great-motorcycle-rides-north-america-f.jpg') }

    let(:createGroup) do
      %(mutation {
        createGroup(
          title: "Dirtbikes",
          description: "We ride dirtbikes"
        ){
          title
          description
        }
      })
    end
    subject(:result) do
      MotorcycleRideSchema.execute(createGroup).as_json
    end

    it "returns new group" do
      group_result = result["data"]["createGroup"]
      expect(group_result["title"]).to eq("Dirtbikes")
      expect(group_result["description"]).to eq("We ride dirtbikes")
    end
  end
end
