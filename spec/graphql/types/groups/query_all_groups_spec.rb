require "rails_helper"

RSpec.describe Types::QueryType do
  describe "groups" do
    let!(:groups) { create_pair(:group) }

    let(:query) do
      %(query {
          allGroups {
              title
              description
          }
      })
    end
    subject(:result) do
      MotorcycleRideSchema.execute(query).as_json
    end

    it "returns all groups" do
      expect(result.dig("data", "allGroups")).to match_array(
        groups.map do |group|
          {
            "title" => group.title,
            "description" => group.description,
          }
        end
      )
    end
  end
end
