require "rails_helper"

RSpec.describe Types::QueryType do
  describe "users" do
    let!(:users) { create_pair(:user) }

    let(:query) do
      %(query {
          allUsers {
              firstName
              lastName
              username
              email
              phone
          }
      })
    end
    subject(:result) do
      MotorcycleRideSchema.execute(query).as_json
    end

    it "returns all users" do
      expect(result.dig("data", "allUsers")).to match_array(
        users.map do |user|
          {
            "firstName" => user.first_name,
            "lastName" => user.last_name,
            "username" => user.username,
            "email" => user.email,
            "phone" => user.phone
          }
        end
      )
    end
  end
end
