require "rails_helper"

RSpec.describe Types::QueryType do
  describe "gets a users bikes" do
    let!(:user) { User.create!(id: 1, first_name: "Kermit", last_name: "The Frog", username: "Kermie", password: 'password', email: "kermit@muppets.com", phone: 1234567890, about: 'timid green rider', avatar: 'https://66.media.tumblr.com/avatar_f5b7729fb645_128.pnj', background_image: 'https://www.motorcycle.com/blog/wp-content/uploads/2017/05/050917-great-motorcycle-rides-north-america-f.jpg') }
    let!(:user2) { User.create!(id: 2, first_name: "Fozzy", last_name: "The Bear", username: "The Fozz", password: 'password', email: "fozzie@muppets.com", phone: 1112223333, about: 'likes casual rides', avatar: 'https://pm1.narvii.com/6300/279717e1eaf7c5ed351ed1272ac99814834a1182_128.jpg', background_image: 'https://visitpa.com/sites/default/master/files/styles/960/public/carousel/feature-2-LP-image.jpg?itok=lw1z_kvP') }
    let!(:user3) { User.create!(id: 3, first_name: "Miss", last_name: "Piggy", username: "Pink Pig", password: 'password', email: "piggy@muppets.com", phone: 1114445555, about: 'dont slow her down', avatar: 'https://pm1.narvii.com/7092/d9c207406796315e5e19389ff94c95066c3c592ar1-474-474v2_128.jpg', background_image: 'http://mag.gothrider.com/wp-content/uploads/2016/06/klickitatloop.jpg') }

    let!(:bike) { Bike.create(id: 1, make: "Yamaha", model: "YZ250", year: 2018, user_id: user.id) }
    let!(:bike2) { Bike.create(id: 2, make: "Honda", model: "CRF250x", year: 1997, user_id: user2.id) }
    let!(:bike3) { Bike.create(id: 3, make: "Kawasaki", model: "KX450F", year: 2008, user_id: user3.id) }

    let(:query) do
      %(query {
        user(id: "2") {
          firstName
          lastName
          username
          email
          phone
          bikes {
            make
            model
            year
          }
        }
      })
    end
    subject(:result) do
      MotorcycleRideSchema.execute(query).as_json
    end

    it "returns bikes for a user" do
      expected = {
        "firstName" => user.first_name,
        "lastName" => user.last_name,
        "username" => user.username,
        "email" => user.email,
        "phone" => user.phone,
        "make" => bike.make,
        "model" => bike.model,
        "year" => bike.year
      }
      user_result = result.dig("data", "user")

      expect(user_result["lastName"]).to eq(user2.last_name)
      expect(user_result["firstName"]).to eq(user2.first_name)
      expect(user_result["username"]).to eq(user2.username)
      expect(user_result["email"]).to eq(user2.email)
      expect(user_result["phone"]).to eq(user2.phone)
      expect(user_result["bikes"][0]["make"]).to eq(bike2.make)
      expect(user_result["bikes"][0]["model"]).to eq(bike2.model)
      expect(user_result["bikes"][0]["year"]).to eq(bike2.year)
    end
  end
end
