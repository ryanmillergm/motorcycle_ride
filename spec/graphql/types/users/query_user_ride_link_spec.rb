require "rails_helper"

RSpec.describe Types::QueryType do
  describe "gets a users rides" do
    let!(:user) { User.create!(id: 1, first_name: "Kermit", last_name: "The Frog", username: "Kermie", password: 'password', email: "kermit@muppets.com", phone: 1234567890, about: 'timid green rider', avatar: 'https://66.media.tumblr.com/avatar_f5b7729fb645_128.pnj', background_image: 'https://www.motorcycle.com/blog/wp-content/uploads/2017/05/050917-great-motorcycle-rides-north-america-f.jpg') }
    let!(:user2) { User.create!(id: 2, first_name: "Fozzy", last_name: "The Bear", username: "The Fozz", password: 'password', email: "fozzie@muppets.com", phone: 1112223333, about: 'likes casual rides', avatar: 'https://pm1.narvii.com/6300/279717e1eaf7c5ed351ed1272ac99814834a1182_128.jpg', background_image: 'https://visitpa.com/sites/default/master/files/styles/960/public/carousel/feature-2-LP-image.jpg?itok=lw1z_kvP') }
    let!(:user3) { User.create!(id: 3, first_name: "Miss", last_name: "Piggy", username: "Pink Pig", password: 'password', email: "piggy@muppets.com", phone: 1114445555, about: 'dont slow her down', avatar: 'https://pm1.narvii.com/7092/d9c207406796315e5e19389ff94c95066c3c592ar1-474-474v2_128.jpg', background_image: 'http://mag.gothrider.com/wp-content/uploads/2016/06/klickitatloop.jpg') }

    let!(:ride) { Ride.create(id: 1, title: 'ride 1', description: 'first ride', distance: 10, ride_category: 'cat 1', duration: 10, date: 'yesterday', image_link: 'pic link', map_link: 'map link') }
    let!(:ride2) { Ride.create(id: 2, title: 'ride 2', description: 'second ride', distance: 15, ride_category: 'cat 2', duration: 15, date: 'today', image_link: 'pic link', map_link: 'map link') }
    let!(:ride3) { Ride.create(id: 3, title: 'ride 3', description: 'third ride', distance: 20, ride_category: 'cat 3', duration: 20, date: 'tomorrow', image_link: 'pic link', map_link: 'map link') }

    let!(:user_ride) { UserRide.create(id: 1, user_id: user.id, ride_id: ride.id) }
    let!(:user_ride1) { UserRide.create(id: 2, user_id: user2.id, ride_id: ride2.id) }
    let!(:user_ride2) { UserRide.create(id: 3, user_id: user3.id, ride_id: ride3.id) }

    let(:query) do
      %(query {
        user(id: "2") {
          firstName
          lastName
          username
          email
          phone
          rides {
            title
            description
            distance
            rideCategory
            duration
            date
            imageLink
            mapLink
          }
        }
      })
    end
    subject(:result) do
      MotorcycleRideSchema.execute(query).as_json
    end

    it "returns groups for a user" do
      expected = {
        "firstName" => user.first_name,
        "lastName" => user.last_name,
        "username" => user.username,
        "email" => user.email,
        "phone" => user.phone,
        "title" => ride.title,
        "description" => ride.description,
        "distance" => ride.distance,
        "rideCategory" => ride.ride_category,
        "duration" => ride.duration,
        "date" => ride.date,
        "imageLink" => ride.image_link,
        "mapLink" => ride.map_link
      }
      user_result = result.dig("data", "user")

      expect(user_result["lastName"]).to eq(user2.last_name)
      expect(user_result["firstName"]).to eq(user2.first_name)
      expect(user_result["username"]).to eq(user2.username)
      expect(user_result["email"]).to eq(user2.email)
      expect(user_result["phone"]).to eq(user2.phone)
      expect(user_result["rides"][0]["title"]).to eq(ride2.title)
      expect(user_result["rides"][0]["description"]).to eq(ride2.description)
      expect(user_result["rides"][0]["distance"]).to eq(ride2.distance)
      expect(user_result["rides"][0]["rideCategory"]).to eq(ride2.ride_category)
      expect(user_result["rides"][0]["duration"]).to eq(ride2.duration)
      expect(user_result["rides"][0]["date"]).to eq(ride2.date)
      expect(user_result["rides"][0]["imageLink"]).to eq(ride2.image_link)
      expect(user_result["rides"][0]["mapLink"]).to eq(ride2.map_link)
    end
  end

  describe "gets a rides users" do
    let!(:user) { User.create!(id: 1, first_name: "Kermit", last_name: "The Frog", username: "Kermie", password: 'password', email: "kermit@muppets.com", phone: 1234567890, about: 'timid green rider', avatar: 'https://66.media.tumblr.com/avatar_f5b7729fb645_128.pnj', background_image: 'https://www.motorcycle.com/blog/wp-content/uploads/2017/05/050917-great-motorcycle-rides-north-america-f.jpg') }
    let!(:user2) { User.create!(id: 2, first_name: "Fozzy", last_name: "The Bear", username: "The Fozz", password: 'password', email: "fozzie@muppets.com", phone: 1112223333, about: 'likes casual rides', avatar: 'https://pm1.narvii.com/6300/279717e1eaf7c5ed351ed1272ac99814834a1182_128.jpg', background_image: 'https://visitpa.com/sites/default/master/files/styles/960/public/carousel/feature-2-LP-image.jpg?itok=lw1z_kvP') }
    let!(:user3) { User.create!(id: 3, first_name: "Miss", last_name: "Piggy", username: "Pink Pig", password: 'password', email: "piggy@muppets.com", phone: 1114445555, about: 'dont slow her down', avatar: 'https://pm1.narvii.com/7092/d9c207406796315e5e19389ff94c95066c3c592ar1-474-474v2_128.jpg', background_image: 'http://mag.gothrider.com/wp-content/uploads/2016/06/klickitatloop.jpg') }

    let!(:ride) { Ride.create(id: 1, title: 'ride 1', description: 'first ride', distance: 10, ride_category: 'cat 1', duration: 10, date: 'yesterday', image_link: 'pic link', map_link: 'map link') }
    let!(:ride2) { Ride.create(id: 2, title: 'ride 2', description: 'second ride', distance: 15, ride_category: 'cat 2', duration: 15, date: 'today', image_link: 'pic link', map_link: 'map link') }
    let!(:ride3) { Ride.create(id: 3, title: 'ride 3', description: 'third ride', distance: 20, ride_category: 'cat 3', duration: 20, date: 'tomorrow', image_link: 'pic link', map_link: 'map link') }

    let!(:user_ride) { UserRide.create(id: 1, user_id: user.id, ride_id: ride.id) }
    let!(:user_ride1) { UserRide.create(id: 2, user_id: user2.id, ride_id: ride2.id) }
    let!(:user_ride2) { UserRide.create(id: 3, user_id: user3.id, ride_id: ride3.id) }

    let(:query) do
      %(query {
        ride(id: "2") {
          title
          description
          distance
          rideCategory
          duration
          date
          imageLink
          mapLink
          users {
            firstName
            lastName
            username
            email
            phone
          }
        }
      })
    end
    subject(:result) do
      MotorcycleRideSchema.execute(query).as_json
    end

    it "returns groups for a user" do
      expected = {
        "firstName" => user.first_name,
        "lastName" => user.last_name,
        "username" => user.username,
        "email" => user.email,
        "phone" => user.phone,
        "title" => ride.title,
        "description" => ride.description,
        "distance" => ride.distance,
        "rideCategory" => ride.ride_category,
        "duration" => ride.duration,
        "date" => ride.date,
        "imageLink" => ride.image_link,
        "mapLink" => ride.map_link
      }
      ride_result = result.dig("data", "ride")

      expect(ride_result["title"]).to eq(ride2.title)
      expect(ride_result["description"]).to eq(ride2.description)
      expect(ride_result["distance"]).to eq(ride2.distance)
      expect(ride_result["rideCategory"]).to eq(ride2.ride_category)
      expect(ride_result["duration"]).to eq(ride2.duration)
      expect(ride_result["date"]).to eq(ride2.date)
      expect(ride_result["imageLink"]).to eq(ride2.image_link)
      expect(ride_result["mapLink"]).to eq(ride2.map_link)
      expect(ride_result["users"][0]["lastName"]).to eq(user2.last_name)
      expect(ride_result["users"][0]["firstName"]).to eq(user2.first_name)
      expect(ride_result["users"][0]["username"]).to eq(user2.username)
      expect(ride_result["users"][0]["email"]).to eq(user2.email)
      expect(ride_result["users"][0]["phone"]).to eq(user2.phone)
    end
  end
end
