require "rails_helper"

RSpec.describe Types::QueryType do
  describe "user can create an account" do

    let(:createUser) do
      %(mutation {
        createUser(
          firstName: "joe",
          lastName: "bob",
          username: "joebob",
          authProvider: {
            email: {
              email: "joebob@gmail.com",
         			password: "password"
            }
          },
          phone: 9327482,
          avatar: "image",
          about: "about",
          backgroundImage: "image"
        ){
          id
          firstName
          lastName
          username
          email
          phone
          avatar
          about
          backgroundImage
        }
      })
    end
    subject(:result) do
      MotorcycleRideSchema.execute(createUser).as_json
    end


    it "returns created user" do
      user_result = result["data"]["createUser"]
      expect(user_result["firstName"]).to eq("joe")
      expect(user_result["lastName"]).to eq("bob")
      expect(user_result["username"]).to eq("joebob")
      expect(user_result["email"]).to eq("joebob@gmail.com")
      expect(user_result["phone"]).to eq(9327482)
    end
  end
end
