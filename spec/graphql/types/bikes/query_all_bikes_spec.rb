require "rails_helper"

RSpec.describe Types::QueryType do
  describe "bikes" do
    let!(:bikes) { create_pair(:bike) }

    let(:query) do
      %(query {
          allBikes {
              make
              model
              year
          }
      })
    end
    subject(:result) do
      MotorcycleRideSchema.execute(query).as_json
    end

    it "returns all bikes" do
      expect(result.dig("data", "allBikes")).to match_array(
        bikes.map do |bike|
          {
            "make" => bike.make,
            "model" => bike.model,
            "year" => bike.year,
          }
        end
      )
    end
  end
end
