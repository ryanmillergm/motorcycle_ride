require "rails_helper"

RSpec.describe Types::QueryType do
  describe "a user can add a bike" do

    let!(:user) { User.create!(id: 1, first_name: "Kermit", last_name: "The Frog", username: "Kermie", password: 'password', email: "kermit@muppets.com", phone: 1234567890, about: 'timid green rider', avatar: 'https://66.media.tumblr.com/avatar_f5b7729fb645_128.pnj', background_image: 'https://www.motorcycle.com/blog/wp-content/uploads/2017/05/050917-great-motorcycle-rides-north-america-f.jpg') }

    let(:createBike) do
      %(mutation {
        createBike(
          make: "Honda",
          model: "CW600",
          year: 1997,
          imageUrl: "image",
          userId: 1
        ){
          make
          model
          year
          imageUrl
        }
      })
    end
    subject(:result) do
      MotorcycleRideSchema.execute(createBike).as_json
    end

    it "returns added bike" do
      bike_result = result["data"]["createBike"]
      expect(bike_result["make"]).to eq("Honda")
      expect(bike_result["model"]).to eq("CW600")
      expect(bike_result["year"]).to eq(1997)
      expect(bike_result["imageUrl"]).to eq("image")
    end
  end
end
