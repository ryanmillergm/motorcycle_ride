# require "rails_helper"
#
# RSpec.describe Types::QueryType do
#   describe "update a bike" do
#     let!(:user) { User.create(id: 1, first_name: "Bob", last_name: "Olson", username: "paulolson", email: "bob@gmail.com", phone: 5555555555, avatar: "image", about: "I am a person", background_image: "image") }
#
#     let!(:bike) { Bike.create(id: 1, make: "Yamaha", model: "YZ250", year: 2018, user_id: user.id, image_url: "image") }
#
#     let(:query) do
#       %(query {
#         bike(id: "2") {
#           make
#           model
#           year
#         }
#       })
#     end
#     subject(:result) do
#       MotorcycleRideSchema.execute(query).as_json
#     end
#
#     it "updates a bike" do
#       expected = {
#         "make" => bike.make,
#         "model" => bike.model,
#         "year" => bike.year,
#       }
#       bike_result = result.dig("data", "bike")
#
#       expect(bike_result["make"]).to eq(bike2.make)
#       expect(bike_result["model"]).to eq(bike2.model)
#       expect(bike_result["year"]).to eq(bike2.year)
#     end
#   end
# end
