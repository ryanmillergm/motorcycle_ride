require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validations' do
    it { should validate_presence_of :first_name }
    it { should validate_presence_of :last_name }
    it { should validate_presence_of :username }
    it { should validate_presence_of :email }
  end

  describe 'relationships' do
    it { should have_many :bikes }
    it { should have_many :user_groups }
    it { should have_many(:groups).through :user_groups}
    it { should have_many :user_rides }
    it { should have_many(:rides).through :user_rides }
    it { should have_many :friends }
  end

end
