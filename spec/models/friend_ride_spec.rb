require "rails_helper"

RSpec.describe FriendRide, type: :model do
  describe 'relationships' do
    it { should belong_to :friend }
    it { should belong_to :ride }
  end
end
