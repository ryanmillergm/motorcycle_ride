require "rails_helper"

RSpec.describe Friend, type: :model do
  describe 'relationships' do
    it { should belong_to :user }
    it { should have_many :friend_rides }
    it { should have_many(:rides).through :friend_rides }
  end

  describe 'validations' do
    it { should validate_presence_of :username }
  end
end
