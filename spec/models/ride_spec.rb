require "rails_helper"

RSpec.describe Ride, type: :model do
  describe 'relationships' do
    it { should have_many :user_rides }
    it { should have_many(:users).through :user_rides }
    it { should have_many :friend_rides }
    it { should have_many(:friends).through :friend_rides }
  end
end
