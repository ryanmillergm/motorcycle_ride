require 'rails_helper'

RSpec.describe Bike, type: :model do
  describe 'validations' do
    it { should validate_presence_of :make }
    it { should validate_presence_of :model }
    it { should validate_presence_of :year }
    it { should validate_presence_of :user_id }
  end

  describe 'relationships' do
    it { should belong_to :user }
  end
end
