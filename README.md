# Motorcycle Ride

This project is the back end for an app where users can find others who ride motorcycles and meet to ride specific routes. A user can find "rides" where people meet and ride motorcycles together. A user can create or join groups and once in a group can chat with each other in a chatroom. The front end project can be found [here](https://github.com/baldm0mma/ride_with_me).

This REST API has several endpoints for basic CRUD functionality of **users**, **bikes**, **rides** and **groups**. Users and rides are joined with **UserRides** with a many to many relationship. All responses are JSON using GraphQL.

The app is deployed at https://dashboard.heroku.com/apps/motorcycle-ride.

This project was part of [Turing School of Software & Design](https://turing.io)'s Back End Engineering program (Mod 4). See the project spec [here](https://backend.turing.io/module4/projects/cross_pollination/cross_pollination_spec). It was completed in 10 days by [Earl Stephens](https://github.com/earl-stephens), [Ryan Miller](https://github.com/ryanmillergm), [Jev Forsberg](https://github.com/baldm0mma) and [Taylor Jordan](https://github.com/TaylorNoelJordan).

View the project board at https://gitlab.com/ryanmillergm/motorcycle_ride/-/boards

## Schema
![schema](./public/images/schema.png)

## Tech Stack
 - Framework: Ruby on Rails v5.2.3
 - Language: Ruby
 - Database: PostgreSQL v7.12.1
 - Query Language: GraphQL v1.8.13
 - Testing: RSpec

## Other Packages
 - Figaro
 - Faraday
 - Active Model Serializers v0.10.0
 - VCR
 - Factory Bot

## Local Setup
 - `$ git clone https://gitlab.com/ryanmillergm/motorcycle_ride.git`
 - `$ cd motorcycle_ride`
 - `$ bundle exec install`
 - `$ bundle exec rails db:create`
 - `$ bundle exec rails db:migrate`
 - `$ bundle exec rails db:seed`
 - `$ bundle exec figaro install`

## Additional steps
 - Delete credentials.yml.enc and master.key files in config  if they exist &
    run `EDITOR="atom --wait" rails credentials:edit`
    This creates your own credentials and master key once you close the credential.edit file.
 - NOTE* this example is specifally if you are using atom as your text editor.

## Running the Server Locally
 - `$ rails s`
 - Access local endpoints at `http://localhost:3000/graphql`
 - Access deployed endpoints at `https://dashboard.heroku.com/apps/motorcycle-ride`

## Running the Test Suite
 - `$ rspec`

## API Endpoints
### List all users in the database
Request:
```
GET /graphql
Body: GraphQL
{
    allUsers {
        id
        firstName
        lastName
        email
        phone
    }
}
```
![Request:](./public/images/get_all_users_request.png)
Example response:
```
Status: 200
Body:
{
    "data": {
        "allUsers": [
            {
                "id": "1",
                "firstName": "joe",
                "lastName": "bob",
                "email": "joebob@gma\nil.com",
                "phone": 5555555555
            },
            {
                "id": "2",
                "firstName": "chris",
                "lastName": "johnson",
                "email": "chris@gmail.com",
                "phone": 34534543534
            }
        ]
    }
}
```
![Response:](./public/images/get_all_users_response.png)
### List one specific user by id in the database
Request:
```
GET /graphql
Body: GraphQL
{
  user(id: "4") {
    firstName
    lastName
  }
}
```
![Request:](./public/images/get_user_by_id_request.png)
Example response:
```
Status: 200
Content-Type: application/json
Body:
{
  "data": {
    "user": {
      "firstName": "Miss",
      "lastName": "Piggy"
    }
  }
}
```
![Response:](./public/images/get_user_by_id_response.png)
### Create a new user
Request:
```
POST /graphql
Body: GraphQL
```
Example response:
```
mutation {
  createUser(
    firstName: "joe",
    lastName: "bob",
    username: "joebob",
    authProvider: {
      email: {
        email: "joebob@gmail.com",
   			password: "password"
      }
    },
    phone: 9327482,
    avatar: "image",
    about: "about",
    backgroundImage: "image"
  ){
    firstName
    lastName
    username
    email
    phone
    avatar
    about
    backgroundImage
  }
}
```
![Request:](./public/images/create_new_user_request.png)
Example successful response:
```
Status: 200
Content-Type: application/json
Body:
{
  "data": {
    "createUser": {
      "firstName": "joe",
      "lastName": "bob",
      "username": "joebob",
      "email": "joebob@gmail.com",
      "phone": 9327482,
      "avatar": "image",
      "about": "about",
      "backgroundImage": "image"
    }
  }
}
```
![Response:](./public/images/create_new_user_response.png)
Failed response (did not include username or email):
```
Status: 400
Body:
{
  "errors": [
    {
      "message": "Field 'createUser' is missing required arguments: username",
      "locations": [
        {
          "line": 2,
          "column": 3
        }
      ],
      "fields": [
        "mutation",
        "createUser"
      ]
    }
  ]
}
```
Failed response (duplicate email):
```
Status: 422
Body:
*UPDATE!!!
{ "error": "That email is already taken" }
```
### Update an existing user by id in the database
Request:
```
GET /graphql
Body: GraphQL
mutation {
  updateUser(
    id: 5
    firstName: "Johnny"
  )
  {
    id
    firstName
    lastName
    username
    email
    phone
    avatar
    about
    backgroundImage
  }
}
```
![Request:](./public/images/update_user_request.png)
Example successful response:
```
{
  "data": {
    "updateUser": {
      "id": "5",
      "firstName": "Johnny",
      "lastName": "Waldorf",
      "username": "Grumpy",
      "email": "grouches@muppets.com",
      "phone": 1118889999,
      "avatar": "https://pm1.narvii.com/7110/b456dd8951e3ae3e8c3969ee2fbe5c101d5fe912r1-1232-816v2_128.jpg",
      "about": "always complaining about other riders",
      "backgroundImage": "https://www.redlandsdailyfacts.com/wp-content/uploads/migration/2016/201609/NEWS_160919986_AR_0_QYVJSPJQZBCE.jpg?w=535"
    }
  }
}
```
![Respnose:](./public/images/update_user_response.png)
```
{

}
```
Example response:
```
Status: 200
Content-Type: application/json
Body:
{

}
```
### Remove a user from the database (based on the id)
Request (enter the id of the user into `:id`):
```
GET /graphql
Body: GraphQL
mutation {
  deleteUser(id: 3)
  {
    id
    firstName
    lastName
    username
    email
    phone
  }
}

```
![Request:](./public/images/delete_user_request.png)
Successful deletion response:
```
{
  "data": {
    "deleteUser": {
      "id": "3",
      "firstName": "Miss",
      "lastName": "Piggy",
      "username": "Pink Pig",
      "email": "piggy@muppets.com",
      "phone": 1114445555
    }
  }
}
```
![Response:](./public/images/update_user_response.png)
Failed deletion response (did not find a user with that id):
```
{
  "error": {
    "message": "Couldn't find User with 'id'=55",
    "backtrace": []
  },
  "data": {}
}
```

### List all bikes in the database
Request:
```
GET /graphql
Body: GraphQL
{
	allBikes {
    make
    model
    year
    imageUrl
    userId
  }
}
```
![Request:](./public/images/get_all_bikes_request.png)
Example response:
```
Status: 200
Content-Type: application/json
Body:
[{
  "data": {
    "allBikes": [
      {
        "make": "Honda",
        "model": "GoldWing",
        "year": 2018,
        "imageUrl": "https://c.allegroimg.com/s128/0cbe01/ad8f58884be1af8e3e9d8a73592c",
        "userId": 2
      },
      {
        "make": "Kawasaki",
        "model": "Ninja",
        "year": 2019,
        "imageUrl": "https://a.allegroimg.com/s128/03656d/c304a5f443d297da339499801e04/STOMPGRIP-NAKLEJKI-NA-BAK-KAWASAKI-NINJA-250R-Waga-z-opakowaniem-1-kg",
        "userId": 3
      }
    ]
  }
}
```
![Request:](./public/images/get_all_bikes_response.png)
### Find a bike by id.
Request:
```
GET /graphql
Body: GraphQL
{
  bike(id: "2") {
    make
    model
    year
    userId
    imageUrl
  }
}
```
![Request:](./public/images/get_bike_by_id_request.png)
Example successful response:
```
Status: 201
Content-Type: application/json
Body:
{
  "data": {
    "bike": {
      "make": "Kawasaki",
      "model": "Ninja",
      "year": 2019,
      "userId": 3,
      "imageUrl": "https://a.allegroimg.com/s128/03656d/c304a5f443d297da339499801e04/STOMPGRIP-NAKLEJKI-NA-BAK-KAWASAKI-NINJA-250R-Waga-z-opakowaniem-1-kg"
    }
  }
}
```
![Request:](./public/images/get_bike_by_id_response.png)
Example failed response (if there is no bike with that id):
```
Status: 404
Content-Type: application/json
Body:
{
  "error": {
    "message": "Couldn't find Bike with 'id'=2888",
    "backtrace": [
    ]
   },
   "data": {}
 }
```
![Response:](./public/images/get_bike_by_id_response.png)
### Deletes a bike from the database
Request:
```
GET /graphql
Body: GraphQL
```
Successful Response:
```
Status: 204
```
Failed Response (if there is no bike found with the IDs provided in the URL):
```
Status: 404
```
### Gets all rides in the database
Request:
```
GET /graphql
Body: GraphQL
{
	allRides {
    title
    description
    distance
    rideCategory
    duration
    date
    imageLink
    mapLink
  }
}

```
![Request:](./public/images/gets_all_rides_request.png)
Example successful response:
```
{
  "data": {
    "updateRide": {
      "title": "Day trip",
      "description": "Ride until you die!",
      "distance": 67,
      "rideCategory": "mostly flat",
      "duration": 3,
      "date": "May 30, 2017",
      "imageLink": "https://cdn2.cycletrader.com/v1/media/5cef0790e357542c64772761.jpg?width=512&height=384&quality=60",
      "mapLink": "https://www.google.com/maps/dir/39.7507704,-104.9968381/39.7620672,-105.0026637/Boulder/Nederland/39.8028778,-105.5012582/39.749115,-105.3979436/39.7541295,-105.2352988/39.7771963,-105.1442524/39.7853669,-105.0797091/39.75059,-104.9970772/@39.875209,-105.393292,11z/data=!3m1!4b1!4m22!4m21!1m0!1m0!1m5!1m1!1s0x876b8d4e278dafd3:0xc8393b7ca01b8058!2m2!1d-105.2705456!2d40.0149856!1m5!1m1!1s0x876bc6c44e45525b:0x8d645c98aaec4cb2!2m2!1d-105.5108312!2d39.9613759!1m0!1m0!1m0!1m0!1m0!1m0!3e0?hl=en"
    }
  }
}
```
![Response:](./public/images/gets_all_rides_response.png)
### Gets ride by id in the database
Request:
```
GET /graphql
Body: GraphQL
{
  ride(id: "2") {
    title
    description
    distance
    rideCategory
    duration
    date
    imageLink
    mapLink
  }
}
```
![Request:](./public/images/gets_ride_by_id_request.png)
Example successful response:
```
{
  "data": {
    "ride": {
      "title": "East CO",
      "description": "open road",
      "distance": 125,
      "rideCategory": "flat",
      "duration": 3,
      "date": "June 6, 2019",
      "imageLink": "https://live.staticflickr.com/3169/2910454212_05e0137d33_z.jpg",
      "mapLink": "https://www.google.com/maps/dir/Denver/Brush/Wray/Burlington/Limon/39.7414289,-104.9385139/@39.5000743,-103.2242958,8.13z/data=!4m33!4m32!1m5!1m1!1s0x876b80aa231f17cf:0x118ef4f8278a36d6!2m2!1d-104.990251!2d39.7392358!1m5!1m1!1s0x876df132ca5c3521:0x2a6e159584ae0314!2m2!1d-103.6238367!2d40.2588686!1m5!1m1!1s0x87738496a267262d:0x3ea4cd376e8bb03f!2m2!1d-102.2232495!2d40.0758231!1m5!1m1!1s0x870cb291012a6035:0x61dc0f7bc348cce2!2m2!1d-102.2693563!2d39.3061082!1m5!1m1!1s0x876d518484bc6433:0xc3630bec75a0f713!2m2!1d-103.6921737!2d39.2638762!1m0!3e0?hl=en"
    }
  }
}
```
![Response:](./public/images/gets_ride_by_id_response.png)
### Update an existing ride by id in the database
Request:
```
GET /graphql
Body: GraphQL
mutation {
  updateRide(
    id: 5
    description: "Ride until you die!"
  )
  {
    title
    description
    distance
    rideCategory
    duration
    date
    imageLink
    mapLink
  }
}  

```
![Request:](./public/images/update_ride_request.png)
Example successful response:
```
{
  "data": {
    "updateRide": {
      "title": "Day trip",
      "description": "Ride until you die!",
      "distance": 67,
      "rideCategory": "mostly flat",
      "duration": 3,
      "date": "May 30, 2017",
      "imageLink": "https://cdn2.cycletrader.com/v1/media/5cef0790e357542c64772761.jpg?width=512&height=384&quality=60",
      "mapLink": "https://www.google.com/maps/dir/39.7507704,-104.9968381/39.7620672,-105.0026637/Boulder/Nederland/39.8028778,-105.5012582/39.749115,-105.3979436/39.7541295,-105.2352988/39.7771963,-105.1442524/39.7853669,-105.0797091/39.75059,-104.9970772/@39.875209,-105.393292,11z/data=!3m1!4b1!4m22!4m21!1m0!1m0!1m5!1m1!1s0x876b8d4e278dafd3:0xc8393b7ca01b8058!2m2!1d-105.2705456!2d40.0149856!1m5!1m1!1s0x876bc6c44e45525b:0x8d645c98aaec4cb2!2m2!1d-105.5108312!2d39.9613759!1m0!1m0!1m0!1m0!1m0!1m0!3e0?hl=en"
    }
  }
}
```
![Response:](./public/images/update_ride_response.png)
### Delete an existing ride by id in the database
Request:
```
GET /graphql
Body: GraphQL
mutation {
  deleteRide(id: 3){
    title
    description
    distance
    rideCategory
    duration
    date
    imageLink
    mapLink
  }
}
```
![Request:](./public/images/delete_ride_request.png)
Example successful response:
```
{
  "data": {
    "deleteRide": {
      "title": "Offroad scramble",
      "description": "hitting some trails in the national forest",
      "distance": 23,
      "rideCategory": "hilly",
      "duration": 8,
      "date": "August 5, 2018",
      "imageLink": "https://media.tacdn.com/media/attractions-splice-spp-674x446/06/70/db/4d.jpg",
      "mapLink": "https://www.google.com/maps/dir/RAM+Off-Road+Park/38.8679183,-104.572663/38.8679183,-104.572663/38.8960754,-104.5529368/38.8964469,-104.4633699/38.9257546,-104.461887/38.9278377,-104.6080097/38.8859373,-104.6098976/38.8767704,-104.5820638/@38.9102708,-104.6618716,11.15z/data=!4m16!4m15!1m5!1m1!1s0x0:0x55831c0ec933b170!2m2!1d-104.587765!2d38.842267!1m0!1m0!1m0!1m0!1m0!1m0!1m0!1m0!3e0?hl=en"
    }
  }
}
```
![Response:](./public/images/delete_ride_response.png)
## Core Contributors
 - Earl Stephens, [@earl-stephens](https://github.com/earl-stephens)
 - Ryan Miller, [@ryanmillergm](https://github.com/ryanmillergm)
 - Jev Forsberg, [@baldm0mma](https://github.com/baldm0mma)
 - Taylor Jordan, [@TaylorNoelJordan](https://github.com/TaylorNoelJordan)

### How to Contribute
 - Fork and clone the [repo](https://gitlab.com/ryanmillergm/motorcycle_ride)
 - Make changes on your fork & push them to GitHub
 - Visit https://gitlab.com/ryanmillergm/motorcycle_ride/pulls and click `New pull request`

## Known Issues
 -
