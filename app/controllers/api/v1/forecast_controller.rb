class Api::V1::ForecastController < ApplicationController
  def index
    render json: ForecastSerializer.new(ForecastFacade.new(forecast_params))
  end

  private

  def forecast_params
    params.permit(:location, :time)
  end
end
