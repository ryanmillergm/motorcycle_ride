class DarkSkyService
  attr_reader :coordinates,
              :time

  def initialize(coordinates, time)
    @coordinates = coordinates
    @time = time
  end

  def get_forecast
    response = fetch_weather_data("https://api.darksky.net/forecast/#{ENV['DARK_SKY_API']}/#{@coordinates[:lat]},#{@coordinates[:lng]},#{@time}")
  end

  private

  def fetch_weather_data(url)
    response = Faraday.get(url)
    JSON.parse(response.body)
  end
end
