class ForecastSerializer
  include FastJsonapi::ObjectSerializer
  attributes :get_forecast,
             :id,
             :location

end
