class MessageSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id,
             :conversation_id,
             :text,
             :created_at
end
