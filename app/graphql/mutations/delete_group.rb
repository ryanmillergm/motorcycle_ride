module Mutations
  class DeleteGroup < BaseMutation
    argument :id, Integer, required: true

    type Types::GroupType

    def resolve(id:)
      Group.find(id).destroy
    end
  end
end
