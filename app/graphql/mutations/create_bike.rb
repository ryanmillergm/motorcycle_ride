module Mutations
  class CreateBike < BaseMutation
    # arguments passed to the `resolved` method
    argument :make, String, required: true
    argument :model, String, required: true
    argument :year, Integer, required: true
    argument :user_id, Integer, required: true
    argument :image_url, String, required: true

    # return type from the mutation
    type Types::BikeType

    def resolve(
      make: nil,
      model: nil,
      year: nil,
      user_id: nil,
      image_url: nil
    )
      Bike.create!(
        make: make,
        model: model,
        year: year,
        user_id: user_id,
        image_url: image_url
      )
    end
  end
end
