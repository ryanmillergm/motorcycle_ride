module Mutations
  class UpdateBike < BaseMutation
    argument :id, Integer, required: true
    argument :make, String, required: false
    argument :model, String, required: false
    argument :year, Integer, required: false
    argument :image_url, String, required: false

    type Types::BikeType

    def resolve(id:, **attributes)
      bike = Bike.find(id)
      bike.update!(attributes)
      bike
    end
  end
end
