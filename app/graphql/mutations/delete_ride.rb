module Mutations
  class DeleteRide < BaseMutation
    argument :id, Integer, required: true

    type Types::RideType


    def resolve(id:)
      ride = Ride.find(id)
      ride.destroy
    end
  end
end
