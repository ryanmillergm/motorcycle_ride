module Mutations
  class UpdateGroup < BaseMutation
    argument :id, Integer, required: true
    argument :title, String, required: false
    argument :description, String, required: false

    type Types::GroupType

    def resolve(id:, **attributes)
      group = Group.find(id)
      group.update!(attributes)
      group
    end
  end
end
