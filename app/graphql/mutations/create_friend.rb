module Mutations
  class CreateFriend < BaseMutation
    argument :username, String, required: true
    argument :avatar, String, required: true
    argument :miles_ridden_together, Integer, required: true
    argument :user_id, Integer, required: true

    type Types::FriendType

    def resolve(username: nil, avatar: nil, user_id: nil)
      Friend.create!(username: username,
                    avatar: avatar,
                    miles_ridden_together: miles_ridden_together,
                    user_id: user_id)
    end
  end
end
