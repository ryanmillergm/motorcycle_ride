module Mutations
  class DeleteFriend < BaseMutation
    argument :id, Integer, required: true

    type Types::FriendType

    def resolve(id:)
      Friend.find(id).destroy
    end
  end
end
