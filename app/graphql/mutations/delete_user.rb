module Mutations
  class DeleteUser < BaseMutation
    argument :id, ID, required: true

    type Types::UserType


    def resolve(id:)
      user = User.find(id)
      user.destroy
    end
  end
end
