module Mutations
  class UpdateRide < BaseMutation
    argument :title, String, required: false
    argument :description, String, required: false
    argument :distance, Integer, required: false
    argument :ride_category, String, required: false
    argument :duration, Integer, required: false
    argument :date, String, required: false
    argument :image_link, String, required: false
    argument :map_link, String, required: false

    argument :id, ID, required: true

    type Types::RideType

    def resolve(id:, **attributes)
      Ride.find(id).tap do |ride|
        ride.update!(attributes)
      end
    end
  end
end
