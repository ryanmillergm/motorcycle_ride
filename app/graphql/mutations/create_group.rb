module Mutations
  class CreateGroup < BaseMutation
    # arguments passed to the `resolved` method
    argument :title, String, required: true
    argument :description, String, required: true

    # return type from the mutation
    type Types::GroupType

    def resolve(
      title: nil,
      description: nil
    )
      Group.create!(
        title: title,
        description: description
      )
    end
  end
end
