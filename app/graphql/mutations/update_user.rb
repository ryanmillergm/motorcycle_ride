module Mutations
  class UpdateUser < BaseMutation
    argument :first_name, String, required: false
    argument :last_name, String, required: false
    argument :username, String, required: false
    argument :phone, Int, required: false
    argument :avatar, String, required: false
    argument :about, String, required: false
    argument :background_image, String, required: false

    argument :id, Integer, required: true

    type Types::UserType

    def resolve(id:, **attributes)
      User.find(id).tap do |user|
        user.update!(attributes)
      end
    end
  end
end
