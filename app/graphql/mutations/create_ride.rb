module Mutations
  class CreateRide < BaseMutation
    # arguments passed to the `resolved` method
    argument :title, String, required: true
    argument :description, String, required: true
    argument :distance, Integer, required: true
    argument :ride_category, String, required: true
    argument :duration, Integer, required: true
    argument :date, String, required: true
    argument :image_link, String, required: true
    argument :map_link, String, required: true

    # return type from the mutation
    type Types::RideType

    def resolve(
      title: nil,
      description: nil,
      distance: nil,
      ride_category: nil,
      duration: nil,
      date: nil,
      image_link: nil,
      map_link: nil
    )
      Ride.create!(
        title: title,
        description: description,
        distance: distance,
        ride_category: ride_category,
        duration: duration,
        date: date,
        image_link: image_link,
        map_link: map_link
      )
    end
  end
end
