module Mutations
  class UpdateFriend < BaseMutation
    argument :id, Integer, required: true
    argument :username, String, required: false
    argument :avatar, String, required: false
    argument :miles_ridden_together, Integer, required: false

    type Types::FriendType

    def resolve(id:, **attributes)
      friend = Friend.find(id)
      friend.update!(attributes)
      friend
    end
  end
end
