module Mutations
  class CreateUser < BaseMutation

    class AuthProviderSignupData < Types::BaseInputObject
      argument :email, Types::AuthProviderEmailInput, required: false
    end

    argument :first_name, String, required: true
    argument :last_name, String, required: true
    argument :username, String, required: true
    argument :phone, Int, required: false
    argument :avatar, String, required: false
    argument :about, String, required: false
    argument :background_image, String, required: false
    argument :auth_provider, AuthProviderSignupData, required: false

    type Types::UserType

    def resolve(
      first_name: nil,
      last_name: nil,
      username: nil,
      email: nil,
      password: nil,
      phone: nil,
      avatar: nil,
      about: nil,
      background_image: nil,
      auth_provider: nil
    )

      User.create!(
        first_name: first_name,
        last_name: last_name,
        username: username,
        email: auth_provider&.[](:email)&.[](:email),
        password: auth_provider&.[](:email)&.[](:password),
        phone: phone,
        avatar: avatar,
        about: about,
        background_image: background_image
      )
    end
  end
end
