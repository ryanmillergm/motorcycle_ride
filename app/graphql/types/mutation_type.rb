module Types
  class MutationType < BaseObject
    field :updateRide, mutation: Mutations::UpdateRide
    field :updateUser, mutation: Mutations::UpdateUser
    field :deleteRide, mutation: Mutations::DeleteRide
    field :deleteUser, mutation: Mutations::DeleteUser
    field :create_user, mutation: Mutations::CreateUser
    field :create_bike, mutation: Mutations::CreateBike
    field :create_group, mutation: Mutations::CreateGroup
    field :create_ride, mutation: Mutations::CreateRide
    field :create_friend, mutation: Mutations::CreateFriend
    field :signin_user, mutation: Mutations::SignInUser
    field :delete_friend, mutation: Mutations::DeleteFriend
    field :update_friend, mutation: Mutations::UpdateFriend
    field :delete_group, mutation: Mutations::DeleteGroup
    field :update_group, mutation: Mutations::UpdateGroup
    field :update_bike, mutation: Mutations::UpdateBike
  end
end
