module Types
  class RideType < BaseObject
    field :id, ID, null: false
    field :title, String, null: false
    field :description, String, null: false
    field :distance, Integer, null: false
    field :ride_category, String, null: false
    field :duration, Integer, null: false
    field :date, String, null: false
    field :image_link, String, null: false
    field :map_link, String, null: false
    field :users, [Types::UserType], null: false
    field :friends, [Types::FriendType], null: false
  end
end
