module Types
  class BikeType < BaseObject
    field :id, ID, null: false
    field :make, String, null: false
    field :model, String, null: false
    field :year, Integer, null: false
    field :user_id, Integer, null: false
    field :image_url, String, null: false
    field :user, UserType, null: false
  end
end
