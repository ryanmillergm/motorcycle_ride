module Types
  class GroupType < BaseObject
    field :id, ID, null: false
    field :title, String, null: false
    field :description, String, null: false
    field :users, [Types::UserType], null: false
  end
end
