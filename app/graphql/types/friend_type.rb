module Types
  class FriendType < BaseObject
    field :id, ID, null: false
    field :username, String, null: false
    field :avatar, String, null: false
    field :miles_ridden_together, Integer, null: false
    field :user_id, Integer, null: false
    field :user, UserType, null: false
    field :rides, [Types::RideType], null: false
  end
end
