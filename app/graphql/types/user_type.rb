module Types
  class UserType < BaseObject
    field :id, ID, null: false
    field :first_name, String, null: false
    field :last_name, String, null: false
    field :username, String, null: false
    field :email, String, null: false
    field :phone, Int, null: false
    field :avatar, String, null: false
    field :about, String, null: false
    field :background_image, String, null: false
    field :friends, [Types::FriendType], null: false
    field :bikes, [Types::BikeType], null: false
    field :groups, [Types::GroupType], null: false
    field :rides, [Types::RideType], null: false
  end
end
