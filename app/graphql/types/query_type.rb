module Types
  class QueryType < Types::BaseObject
    field :all_users, [UserType], null: false
    field :user, Types::UserType, null: false do
      argument :id, ID, required: true
    end

    field :all_bikes, [BikeType], null: false
    field :bike, Types::BikeType, null: false do
      argument :id, ID, required: true
    end

    field :all_groups, [GroupType], null: false
    field :group, Types::GroupType, null: false do
      argument :id, ID, required: true
    end

    field :all_rides, [RideType], null: false
    field :ride, Types::RideType, null: false do
      argument :id, ID, required: true
    end

    field :all_friends, [FriendType], null: false
    field :friend, Types::FriendType, null: false do
      argument :id, ID, required: true
    end

    field :me, Types::UserType, null: true

    def all_users
      User.all
    end

    def user(id:)
      User.find(id)
    end

    def all_bikes
      Bike.all
    end

    def bike(id:)
      Bike.find(id)
    end

    def all_groups
      Group.all
    end

    def group(id:)
      Group.find(id)
    end

    def all_rides
      Ride.all
    end

    def ride(id:)
      Ride.find(id)
    end

    def all_friends
      Friend.all
    end

    def friend(id:)
      Friend.find(id)
    end

    def me
      context[:current_user]
    end
  end
end
