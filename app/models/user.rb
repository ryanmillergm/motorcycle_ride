class User < ApplicationRecord
  has_secure_password

  validates_presence_of :first_name
  validates_presence_of :last_name
  validates_presence_of :username
  validates_presence_of :email
  validates :username, uniqueness: true
  validates :email, uniqueness: true
  has_many :bikes, dependent: :destroy
  has_many :user_groups, dependent: :destroy
  has_many :groups, through: :user_groups
  has_many :user_rides, dependent: :destroy
  has_many :rides, through: :user_rides
  has_many :friends, dependent: :destroy
end
