class Bike < ApplicationRecord
  validates_presence_of :make
  validates_presence_of :model
  validates_presence_of :year
  validates_presence_of :user_id
  belongs_to :user
end
