class FriendRide < ApplicationRecord
  belongs_to :friend
  belongs_to :ride
end
