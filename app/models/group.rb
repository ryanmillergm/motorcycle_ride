class Group < ApplicationRecord
  validates_presence_of :title
  validates_presence_of :description
  has_many :user_groups
  has_many :users, through: :user_groups, dependent: :destroy
end
