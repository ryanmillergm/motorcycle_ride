class Ride < ApplicationRecord
  has_many :user_rides, dependent: :destroy
  has_many :users, through: :user_rides
  has_many :friend_rides
  has_many :friends, through: :friend_rides
end
