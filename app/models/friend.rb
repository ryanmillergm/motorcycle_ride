class Friend < ApplicationRecord
  validates_presence_of :username
  belongs_to :user
  has_many :friend_rides
  has_many :rides, through: :friend_rides
end
