class ForecastFacade
  attr_reader :location,
              :time,
              :id

  def initialize(location)
    @location = location["location"]
    @time = location["time"]
    @id = "Future Forecast"
    @coordinates
  end

  def get_forecast
    @coordinates = get_coordinates
    response = dark_sky_service.get_forecast
    forecast = Forecast.new(response["currently"])
  end

  def get_coordinates
    geocoding_service.get_coordinates
  end

  private

  def geocoding_service
    @geocoding ||= GoogleGeocodingService.new(@location)
  end

  def dark_sky_service
    @dark_sky_service ||= DarkSkyService.new(@coordinates, @time)
  end
end
